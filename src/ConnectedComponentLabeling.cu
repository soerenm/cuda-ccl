﻿#include "ConnectedComponentLabeling.h"

#include <opencv2/core/cuda_stream_accessor.hpp>
#include <opencv2/cudev/common.hpp>

constexpr int THREAD_BLOCK_SIZE = 16;
constexpr int MAX_NUMBER_OF_THREADS = 1024;

constexpr int MARK_ROOT_BIN_IMAGE = 128;

__device__ int findRoot(const int* labels, int id) {
    while (labels[id] != id) {
        id = labels[id];
    }
    return id;
}

__device__ void merge(int* labels, unsigned val1, unsigned val2) {
    bool done = false;
    
    while (!done) {

        val1 = findRoot(labels, val1);
        val2 = findRoot(labels, val2);

        if (val1 < val2) {
            int oldVal = atomicMin(labels + val2, val1);
            done = oldVal == val2;
            val2 = oldVal;
        }
        else if (val2 < val1) {
            int oldVal = atomicMin(labels + val1, val2);
            done = oldVal == val1;
            val1 = oldVal;
        }
        else {
            done = true;
        }
    }
}

__device__ int findRoot(const cv::cuda::PtrStepSzi labels, int id) {
    const int width = labels.cols;
    
    int col = id % width;
    int row = id / width;
    while (labels(row, col) != id) {
        id = labels(id / width, id % width);
        col = id % width;
        row = id / width;
    }
    return id;
}

__device__ void merge(cv::cuda::PtrStepSzi labelImg, int val1, int val2) {
    const int width = labelImg.cols;
    bool done = false;

    while (!done) {
        val1 = findRoot(labelImg, val1);
        val2 = findRoot(labelImg, val2);

        if (val1 < val2) {
            int oldVal = atomicMin(&labelImg(val2 / width, val2 % width), val1);
            done = oldVal == val2;
            val2 = oldVal;
        }
        else if (val2 < val1) {
            int oldVal = atomicMin(&labelImg(val1 / width, val1 % width), val2);
            done = oldVal == val1;
            val1 = oldVal;
        }
        else {
            done = true;
        }
    }
}

__global__ void localLabeling(const cv::cuda::PtrStepSzb binImg, cv::cuda::PtrStepi labelImg, ccl::ConnectedComponentLabeling::Connectivity connectivity
#ifndef NDEBUG
    ,cv::cuda::PtrStepi afterRowScan, cv::cuda::PtrStepi afterTopRightScan, cv::cuda::PtrStepi afterColScan,
    cv::cuda::PtrStepi afterTopLeftScan, cv::cuda::PtrStepi afterRowColScanFindRoot,
    cv::cuda::PtrStepi afterRefinement, cv::cuda::PtrStepi afterRefinementFindRoot
#endif
)
{
    int xTotal = threadIdx.x + blockIdx.x * blockDim.x;
    int yTotal = threadIdx.y + blockIdx.y * blockDim.y;

    int tid = threadIdx.x + threadIdx.y * THREAD_BLOCK_SIZE;
    
    __shared__ int img[THREAD_BLOCK_SIZE * THREAD_BLOCK_SIZE];
    __shared__ int labels[THREAD_BLOCK_SIZE * THREAD_BLOCK_SIZE];

    if (xTotal < binImg.cols && yTotal < binImg.rows)
    {
        // ******************************************************
        // Load image and set label ids
        // ******************************************************

        img[tid] = binImg(yTotal, xTotal);
        if(0 != img[tid]) {
            labels[tid] = tid;
        }
        else {
            labels[tid] = 0;
        }
        __syncthreads();

        if (0 != img[tid]) {
            // ******************************************************
            // Row scan
            // ******************************************************
            if (0 != threadIdx.x && img[tid] == img[tid-1]) {
                labels[tid] = labels[tid-1];
            }
#ifndef NDEBUG
            afterRowScan(yTotal, xTotal) = labels[tid] + 1;
#endif
            __syncthreads();

            // ******************************************************
            // Top right (norht east) scan
            // ******************************************************
            if (ccl::ConnectedComponentLabeling::Connectivity::eight == connectivity && THREAD_BLOCK_SIZE-1 != threadIdx.x &&
                0 != threadIdx.y && img[tid] == img[tid - THREAD_BLOCK_SIZE + 1]) {
                labels[tid] = labels[tid - THREAD_BLOCK_SIZE + 1];
            }
#ifndef NDEBUG
            afterTopRightScan(yTotal, xTotal) = labels[tid] + 1;
#endif
            __syncthreads();

            // ******************************************************
            // Column scan
            // ******************************************************
            if (0 != threadIdx.y && img[tid] == img[tid - THREAD_BLOCK_SIZE]) {
                labels[tid] = labels[tid - THREAD_BLOCK_SIZE];
            }
#ifndef NDEBUG
            afterColScan(yTotal, xTotal) = labels[tid] + 1;
#endif
            __syncthreads();

            // ******************************************************
            // Top left (north west) scan
            // ******************************************************
            if (ccl::ConnectedComponentLabeling::Connectivity::eight == connectivity && 0 != threadIdx.x &&
                0 != threadIdx.y && img[tid] == img[tid - THREAD_BLOCK_SIZE - 1]) {
                labels[tid] = labels[tid - THREAD_BLOCK_SIZE - 1];
            }
            __syncthreads();

#ifndef NDEBUG
            afterTopLeftScan(yTotal, xTotal) = labels[tid] + 1;
#endif

            // ******************************************************
            // find root
            // ******************************************************
            labels[tid] = findRoot(&labels[0], tid);
            __syncthreads();
#ifndef NDEBUG
            afterRowColScanFindRoot(yTotal, xTotal) = labels[tid] + 1;
#endif

            // ******************************************************
            // Refinement
            // ******************************************************
            // Row scan
            if (0 < threadIdx.x && img[tid] == img[tid - 1]) {
                merge(&labels[0], tid, tid - 1);
            }            
            __syncthreads();
            // Top right (norht east) scan
            if (ccl::ConnectedComponentLabeling::Connectivity::eight == connectivity && THREAD_BLOCK_SIZE-1 != threadIdx.x &&
                0 != threadIdx.y && img[tid] == img[tid - THREAD_BLOCK_SIZE + 1]) {
                merge(&labels[0], tid, tid - THREAD_BLOCK_SIZE + 1);
            }
            __syncthreads();
#ifndef NDEBUG
            afterRefinement(yTotal, xTotal) = labels[tid] + 1;
#endif
            
            // ******************************************************
            // find root
            // ******************************************************
            labels[tid] = findRoot(&labels[0], tid);
#ifndef NDEBUG
                afterRefinementFindRoot(yTotal, xTotal) = labels[tid] + 1;
#endif

            // ******************************************************
            // To global index
            // ******************************************************
            labelImg(yTotal, xTotal) = blockIdx.x * blockDim.x + labels[tid] % THREAD_BLOCK_SIZE +
                                        (blockIdx.y * blockDim.y + labels[tid] / THREAD_BLOCK_SIZE) * binImg.cols;            
        }
        else {
            labelImg(yTotal, xTotal) = labels[tid]; // Write background zero
#ifndef NDEBUG
            afterRowScan(yTotal, xTotal) = labels[tid];
            afterTopRightScan(yTotal, xTotal) = labels[tid];
            afterColScan(yTotal, xTotal) = labels[tid];
            afterTopLeftScan(yTotal, xTotal) = labels[tid];
            afterRowColScanFindRoot(yTotal, xTotal) = labels[tid];
            afterRefinement(yTotal, xTotal) = labels[tid];
            afterRefinementFindRoot(yTotal, xTotal) = labels[tid];
#endif
        }
    }
    return;
}

__global__ void blockMergeHorizontal(const cv::cuda::PtrStepSzb binImg, cv::cuda::PtrStepSzi labelImg, ccl::ConnectedComponentLabeling::Connectivity connectivity) {
    const int rows = binImg.rows;
    const int cols = binImg.cols;
    // Horizontal Merge
    {
        const int col = threadIdx.x + blockDim.x * blockIdx.x;
        const int row = THREAD_BLOCK_SIZE * (blockIdx.y + 1);
        
        if (row < rows && col < cols) {
            if(ccl::ConnectedComponentLabeling::Connectivity::four == connectivity) {
                if (0 != binImg(row, col) && 0 != binImg(row - 1, col)) {
                    merge(labelImg, labelImg(row, col), labelImg(row - 1, col));
                }
            }
            else {
                if (0 != col && 0 != binImg(row, col) && 0 != binImg(row - 1, col - 1)) {
                    merge(labelImg, labelImg(row, col), labelImg(row - 1, col - 1));
                    if (cols - 1 != col && 0 != binImg(row, col) && 0 != binImg(row - 1, col + 1)) {
                        merge(labelImg, labelImg(row, col), labelImg(row - 1, col + 1));
                    }
                }
                else if (0 != binImg(row, col) && 0 != binImg(row - 1, col)) {
                    merge(labelImg, labelImg(row, col), labelImg(row - 1, col));
                }
                else if (cols - 1 != col && 0 != binImg(row, col) && 0 != binImg(row - 1, col + 1)) {
                    merge(labelImg, labelImg(row, col), labelImg(row - 1, col + 1));
                }
            }
        }
    }
    return;
}

__global__ void blockMergeVertical(const cv::cuda::PtrStepSzb binImg, cv::cuda::PtrStepSzi labelImg, ccl::ConnectedComponentLabeling::Connectivity connectivity) {
    const int rows = binImg.rows;
    const int cols = binImg.cols;
    // Vertical Merge
    {
        const int col = THREAD_BLOCK_SIZE * (blockIdx.x + 1);
        const int row = threadIdx.y + THREAD_BLOCK_SIZE * blockIdx.y;
        if (row < rows && col < cols) {
            if (ccl::ConnectedComponentLabeling::Connectivity::four == connectivity) {
                if(0 != binImg(row, col) && 0 != binImg(row, col - 1)) {
                    merge(labelImg, labelImg(row, col), labelImg(row, col - 1));
                }
            }
            else {
                if (0 != row && 0 != binImg(row, col) && 0 != binImg(row - 1, col - 1)) {
                    merge(labelImg, labelImg(row, col), labelImg(row - 1, col - 1));
                    if (rows - 1 != row && 0 != binImg(row, col) && 0 != binImg(row + 1, col - 1)) {
                        merge(labelImg, labelImg(row, col), labelImg(row + 1, col - 1));
                    }
                }
                else if(0 != binImg(row, col) && 0 != binImg(row, col - 1)) {
                    merge(labelImg, labelImg(row, col), labelImg(row, col - 1));
                }
                else if (rows - 1 != row && 0 != binImg(row, col) && 0 != binImg(row + 1, col - 1)) {
                    merge(labelImg, labelImg(row, col), labelImg(row + 1, col - 1));
                }
            }
            
        }
    }
    return;
}

__global__ void equalLabels(cv::cuda::PtrStepSzi labelImg) {
    int xTotal = threadIdx.x + blockIdx.x * blockDim.x;
    int yTotal = threadIdx.y + blockIdx.y * blockDim.y;

    if (xTotal < labelImg.cols && yTotal < labelImg.rows)
    {
        if (0 != labelImg(yTotal, xTotal)) {
            labelImg(yTotal, xTotal) = findRoot(labelImg, labelImg(yTotal, xTotal));
        }
    }
}

__global__ void compressLabels(cv::cuda::PtrStepSzb binImg, cv::cuda::PtrStepi labelImg, int* numberOfConnectedCompontentsGPU) {
    int xTotal = threadIdx.x + blockIdx.x * blockDim.x;
    int yTotal = threadIdx.y + blockIdx.y * blockDim.y;

    if (xTotal < binImg.cols && yTotal < binImg.rows)
    {
        if (0 != binImg(yTotal, xTotal)) {
            int yTotalTemp = labelImg(yTotal, xTotal) / binImg.cols;
            int xTotalTemp = labelImg(yTotal, xTotal) % binImg.cols;
            if (yTotal == yTotalTemp && xTotal == xTotalTemp) {
                labelImg(yTotal, xTotal) = atomicAdd(numberOfConnectedCompontentsGPU, 1);
                binImg(yTotal, xTotal) = MARK_ROOT_BIN_IMAGE;
            }
        }
    }
}

__global__ void updateGlobalLabelMap(cv::cuda::PtrStepSzb binImg, cv::cuda::PtrStepSzi labelImg) {
    int xTotal = threadIdx.x + blockIdx.x * blockDim.x;
    int yTotal = threadIdx.y + blockIdx.y * blockDim.y;

    if (xTotal < labelImg.cols && yTotal < labelImg.rows)
    {
        if (0 != binImg(yTotal, xTotal) && MARK_ROOT_BIN_IMAGE != binImg(yTotal, xTotal)) {
            int xTotalTemp = labelImg(yTotal, xTotal) % binImg.cols; int yTotalTemp = labelImg(yTotal, xTotal) / binImg.cols;
            labelImg(yTotal, xTotal) = labelImg(yTotalTemp, xTotalTemp);
        }
    }
}

__global__ void initArrayToDetermineBoundingBoxes(const cv::cuda::PtrStepSzb binImg, int* d_xStart, int* d_yStart, int* d_xEnd, int* d_yEnd, int* d_defectivePixel) {
    int tid = threadIdx.x + blockDim.x * blockIdx.x;

    d_xStart[tid] = binImg.cols;
    d_yStart[tid] = binImg.rows;
    d_xEnd[tid] = 0;
    d_yEnd[tid] = 0;
    d_defectivePixel[tid] = 0;
}

// Idea from: https://stackoverflow.com/questions/57416374/cuda-bounding-box-with-npp-labelmarkers
__global__ void boundingBox(const cv::cuda::PtrStepSzb binImg, cv::cuda::PtrStepi labelImg, int* d_xStart, int* d_yStart, int* d_xEnd, int* d_yEnd, int *d_defectivePixel) {
    int xTotal = threadIdx.x + blockIdx.x * blockDim.x;
    int yTotal = threadIdx.y + blockIdx.y * blockDim.y;

    if (xTotal < binImg.cols && yTotal < binImg.rows)
    {
        int labelId = labelImg(yTotal, xTotal);
        if (0 != labelId) {
            if(d_xStart[labelId] > xTotal) {
                atomicMin(&d_xStart[labelId], xTotal);
            }
            if (d_yStart[labelId] > yTotal) {
                atomicMin(&d_yStart[labelId], yTotal);
            }

            if(d_xEnd[labelId] < xTotal) {
                atomicMax(&d_xEnd[labelId], xTotal);
            }
            if(d_yEnd[labelId] < yTotal) {
                atomicMax(&d_yEnd[labelId], yTotal);
            }

            atomicAdd(&d_defectivePixel[labelId], 1);
        }
    }
}


ccl::ConnectedComponentLabeling::ConnectedComponentLabeling(cv::Size maxSize) :
	memoryLabelImg{ maxSize, CV_32SC1},
    bufferSizeResults{ maxSize.area()/2 }
{ // Throwing an exception (in the constructor) is still a potential memory leak!
    CV_CUDEV_SAFE_CALL(cudaMalloc(&d_xStart, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMalloc(&d_yStart, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMalloc(&d_xEnd, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMalloc(&d_yEnd, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMalloc(&d_defectivePixel, bufferSizeResults * sizeof(int)));

    CV_CUDEV_SAFE_CALL(cudaMallocHost(&h_xStart, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMallocHost(&h_yStart, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMallocHost(&h_xEnd, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMallocHost(&h_yEnd, bufferSizeResults * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMallocHost(&h_defectivePixel, bufferSizeResults * sizeof(int)));

    CV_CUDEV_SAFE_CALL(cudaMalloc(&d_numberOfConnectedCompontents, 1 * sizeof(int)));
    CV_CUDEV_SAFE_CALL(cudaMallocHost(&h_numberOfConnectedCompontents, 1 * sizeof(int)));
}
ccl::ConnectedComponentLabeling::~ConnectedComponentLabeling() {
    CV_CUDEV_SAFE_CALL(cudaFree(d_xStart));
    CV_CUDEV_SAFE_CALL(cudaFree(d_yStart));
    CV_CUDEV_SAFE_CALL(cudaFree(d_xEnd));
    CV_CUDEV_SAFE_CALL(cudaFree(d_yEnd));
    CV_CUDEV_SAFE_CALL(cudaFree(d_defectivePixel));

    CV_CUDEV_SAFE_CALL(cudaFree(h_xStart));
    CV_CUDEV_SAFE_CALL(cudaFree(h_yStart));
    CV_CUDEV_SAFE_CALL(cudaFree(h_xEnd));
    CV_CUDEV_SAFE_CALL(cudaFree(h_yEnd));
    CV_CUDEV_SAFE_CALL(cudaFree(h_defectivePixel));
    
    CV_CUDEV_SAFE_CALL(cudaFree(d_numberOfConnectedCompontents));
    CV_CUDEV_SAFE_CALL(cudaFree(h_numberOfConnectedCompontents));
}

std::vector<ccl::ResultFromCCL> ccl::ConnectedComponentLabeling::perform(const cv::cuda::GpuMat& binImg, const ccl::ConnectedComponentLabeling::Connectivity connectiviy, cv::cuda::Stream cudaStreamCV)
{
    if (1 != binImg.channels()) {
        CV_Error(cv::Error::BadNumChannels, "Image has more than one channel.");
    } else if (binImg.rows > memoryLabelImg.rows) {
        CV_Error(cv::Error::BadImageSize, "Row size of binary image is greater than of preallocated image. Check constructor parameter.");
    } else if (binImg.cols > memoryLabelImg.cols) {
        CV_Error(cv::Error::BadImageSize, "Column size of binary image is greater than of preallocated image. Check constructor parameter.");
    }
	cv::cuda::GpuMat labelImg = cv::cuda::GpuMat(memoryLabelImg, cv::Rect{ 0, 0, binImg.cols, binImg.rows });
	cudaStream_t cudaStream = cv::cuda::StreamAccessor::getStream(cudaStreamCV);

    // ******************************************************
    // Local (Block) Labeling
    // ******************************************************
#ifndef NDEBUG
    cv::cuda::GpuMat afterRowScanGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);
    cv::cuda::GpuMat afterTopRightScanGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);
    cv::cuda::GpuMat afterColScanGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);
    cv::cuda::GpuMat afterTopLeftScanGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);

    cv::cuda::GpuMat afterRowColScanFindRootGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);
    cv::cuda::GpuMat afterRefinementGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);
    cv::cuda::GpuMat afterRefinementFindRootGPU = cv::cuda::GpuMat({ binImg.cols, binImg.rows }, CV_32SC1);
#endif
    {
        dim3 block(THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE);
        dim3 grid((binImg.cols + block.x - 1) / block.x, (binImg.rows + block.y - 1) / block.y);

        localLabeling << <grid, block, 0, cudaStream >> > (binImg, labelImg, connectiviy
#ifndef NDEBUG
            , afterRowScanGPU, afterTopRightScanGPU, afterColScanGPU, afterTopLeftScanGPU, afterRowColScanFindRootGPU,
            afterRefinementGPU, afterRefinementFindRootGPU
#endif      
            );
    }
#ifndef NDEBUG
    cv::Mat binImgCPU(binImg);
    cv::Mat localLabelImg(labelImg);

    cv::Mat lafterRowScan(afterRowScanGPU);
    cv::Mat afterTopRightScan(afterTopRightScanGPU);
    cv::Mat afterColScan(afterColScanGPU);
    cv::Mat afterTopLeftScan(afterTopLeftScanGPU);

    cv::Mat afterRowColScanFindRoot(afterRowColScanFindRootGPU);
    cv::Mat afterRefinement(afterRefinementGPU);
    cv::Mat afterRefinementFindRoot(afterRefinementFindRootGPU);

#endif
    // ******************************************************
    // Block Merge (Horizontal and Vertical)
    // ******************************************************
    {
        const int threadsForMerge = THREAD_BLOCK_SIZE;
        const int gridY = static_cast<int>(std::ceil(static_cast<double>(labelImg.rows) / THREAD_BLOCK_SIZE)) - 1;
        const int gridX = static_cast<int>(std::ceil(static_cast<double>(labelImg.cols) / threadsForMerge));
        dim3 block(threadsForMerge, 1);
        dim3 grid(gridX, gridY);
        blockMergeHorizontal << <grid, block, 0, cudaStream >> > (binImg, labelImg, connectiviy);
    }
#ifndef NDEBUG
    cv::Mat localLabelImgHorzMerge(labelImg);
#endif
    {
        const int threadsForMerge = THREAD_BLOCK_SIZE;
        const int gridY = static_cast<int>(std::ceil(static_cast<double>(labelImg.rows) / threadsForMerge));
        const int gridX = static_cast<int>(std::ceil(static_cast<double>(labelImg.cols) / THREAD_BLOCK_SIZE)) - 1;
        dim3 block(1, threadsForMerge);
        dim3 grid(gridX, gridY);
        blockMergeVertical << <grid, block, 0, cudaStream >> > (binImg, labelImg, connectiviy);
    }
#ifndef NDEBUG
    cv::Mat localLabelImgVectMerge(labelImg);   
#endif
    // ******************************************************
    // Equalize IDs (all connected pixels have the same label id)
    // ******************************************************
    {
        dim3 block(THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE);
        dim3 grid((binImg.cols + block.x - 1) / block.x, (binImg.rows + block.y - 1) / block.y);
        equalLabels << <grid, block, 0, cudaStream >> > (labelImg);
    }
#ifndef NDEBUG
    cv::Mat localLabelEqualCCL(labelImg);
#endif
    // ******************************************************
    // Compress and upade Labels, so that all pixel have the same id
    // but with a value smaller than number of connected
    // components in the image. Mark root pixel in binImage with a
    // specified constant.
    // ******************************************************
    {
        const int startIdx = 1;
        cudaMemcpyAsync(d_numberOfConnectedCompontents, &startIdx, 1 * sizeof(int), cudaMemcpyHostToDevice, cudaStream);      
        dim3 block(THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE);
        dim3 grid((binImg.cols + block.x - 1) / block.x, (binImg.rows + block.y - 1) / block.y);
        compressLabels << <grid, block, 0, cudaStream >> > (binImg, labelImg, d_numberOfConnectedCompontents);        
        
        cudaMemcpyAsync(h_numberOfConnectedCompontents, d_numberOfConnectedCompontents, 1 * sizeof(int), cudaMemcpyDeviceToHost, cudaStream);
    }
#ifndef NDEBUG
    cv::Mat labelCompressed(labelImg);
    cv::Mat binImgAfterLabelCompressed(binImg);
#endif
    {
        dim3 block(THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE);
        dim3 grid((binImg.cols + block.x - 1) / block.x, (binImg.rows + block.y - 1) / block.y);
        updateGlobalLabelMap << <grid, block, 0, cudaStream >> > (binImg, labelImg);
    }
#ifndef NDEBUG
    cv::Mat labelImgAfterGlobalLabelCPU(labelImg);
#endif
    // ******************************************************
    // Determine bounding boxes and create result vector on host
    // ******************************************************
    {
        cudaStreamSynchronize(cudaStream);
        dim3 block(std::min(*h_numberOfConnectedCompontents, MAX_NUMBER_OF_THREADS));
        dim3 grid(static_cast<unsigned int>(std::ceil(static_cast<double>(*h_numberOfConnectedCompontents) / MAX_NUMBER_OF_THREADS)));
        initArrayToDetermineBoundingBoxes << <grid, block, 0, cudaStream >> > (binImg, d_xStart, d_yStart, d_xEnd, d_yEnd, d_defectivePixel);
    }
    {
        dim3 block(THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE);
        dim3 grid((binImg.cols + block.x - 1) / block.x, (binImg.rows + block.y - 1) / block.y);
        boundingBox << <grid, block, 0, cudaStream >> > (binImg, labelImg, d_xStart, d_yStart, d_xEnd, d_yEnd, d_defectivePixel);

        const int bytesToSetMem = *h_numberOfConnectedCompontents * sizeof(int);
        cudaMemcpyAsync(h_xStart, d_xStart, bytesToSetMem, cudaMemcpyDeviceToHost, cudaStream);
        cudaMemcpyAsync(h_yStart, d_yStart, bytesToSetMem, cudaMemcpyDeviceToHost, cudaStream);
        cudaMemcpyAsync(h_xEnd, d_xEnd, bytesToSetMem, cudaMemcpyDeviceToHost, cudaStream);
        cudaMemcpyAsync(h_yEnd, d_yEnd, bytesToSetMem, cudaMemcpyDeviceToHost, cudaStream);
        cudaMemcpyAsync(h_defectivePixel, d_defectivePixel, bytesToSetMem, cudaMemcpyDeviceToHost, cudaStream);

    }
    cudaStreamSynchronize(cudaStream);
    
    std::vector<ccl::ResultFromCCL> result(*h_numberOfConnectedCompontents);
    // Label-IDs start with index 1. Set first value to image dimension (like openCV's connectedComponentsWithStats(...))
    result[0].boundingBox.x = 0; result[0].boundingBox.y = 0;
    result[0].boundingBox.width = labelImg.cols; result[0].boundingBox.height = labelImg.rows;
    for (int i = 1; i < *h_numberOfConnectedCompontents; ++i) {
        result[i].boundingBox.x = h_xStart[i];
        result[i].boundingBox.y = h_yStart[i];
        // right and bottom boundaries are not inclusive in cv::Rect
        result[i].boundingBox.width = h_xEnd[i] - h_xStart[i] + 1; 
        result[i].boundingBox.height = h_yEnd[i] - h_yStart[i] + 1;
        result[i].defectivePixels = h_defectivePixel[i];
        //printf("x: %d, y: %d, xEnd: %d, yEnd: %d\n", h_xStart[i], h_yStart[i], h_xEnd[i], h_yEnd[i]);
    }
	return result;
}