#include <iostream>

#include "opencv2/opencv.hpp"
#include "cuda_runtime.h"

#include "ConnectedComponentLabeling.h"

bool compareWithOpenCV(const cv::Mat& binImage, std::vector<ccl::ResultFromCCL> labelGPU) {
	cv::Mat labels, stats, centroids;
	cv::connectedComponentsWithStats(binImage, labels, stats, centroids, 8);

	// openCV gives sometimes "wrong" results at the first index (instead of complete image dimensions)
	int row = 0;
	if (0 != stats.at<int>(0, 0) || 0 != stats.at<int>(0, 1)) {
		row = 1;
		if (!(binImage.cols == stats.at<int>(row, 2) && binImage.rows == stats.at<int>(row, 3))) {
			labelGPU.erase(labelGPU.begin());
		}
	}
	
	bool error = false;
	for (; row < stats.rows; ++row) {
		cv::Rect2i toFindRect{ stats.at<int>(row, 0), stats.at<int>(row, 1) , stats.at<int>(row, 2), stats.at<int>(row, 3) };
		auto iterEl = std::find_if(labelGPU.begin(), labelGPU.end(), [&toFindRect](ccl::ResultFromCCL el) {return el.boundingBox == toFindRect; });
		if (labelGPU.end() == iterEl) {
			error = true;
			std::cerr << "ERROR - Did not find openCV-Rect : x =" << toFindRect.x << ", y =" << toFindRect.y << ", w =" << toFindRect.width << ", h =" << toFindRect.height << " in GPU-Results\n";
		}
		else {
			labelGPU.erase(iterEl);
		}
	}

	if (!labelGPU.empty()) {
		error = true;
		std::cerr << "ERROR - GPU-Results vector has still elements after comparing with openCV function.\n";
	}
	if (!error) {
		std::cout << "Successful - GPU and openCV-Results are equal.\n";
	}
	return error;
}

int main()
{
	std::cout << "OpenCV version : " << CV_VERSION << '\n';
	std::cout << "Major version : " << CV_MAJOR_VERSION << '\n';
	std::cout << "Minor version : " << CV_MINOR_VERSION << '\n';
	std::cout << "Subminor version : " << CV_SUBMINOR_VERSION << '\n';

	int driverVersion, runtimeVersion;
	cudaDriverGetVersion(&driverVersion);
	cudaRuntimeGetVersion(&runtimeVersion);

	std::cout << "Cuda driver version : " << driverVersion << '\n';
	std::cout << "Cuda Runtime version : " << runtimeVersion << '\n';

	// Testmatrix 01
	/**/
	cv::Mat binImage = cv::Mat::zeros({32, 32}, CV_8UC1);
	cv::rectangle(binImage, { 2, 2 }, { 5, 5 }, { 255 }, -1);
	cv::rectangle(binImage, { 18, 18 }, { 25, 25 }, { 255 }, -1);
	
	binImage.at<uchar>(7, 8) = 255;
	binImage.at<uchar>(8, 7) = 255;
	binImage.at<uchar>(9, 10) = 255;
	binImage.at<uchar>(9, 11) = 255;
	cv::rectangle(binImage, { 8, 8 }, { 9, 9 }, { 255 }, -1);
	cv::rectangle(binImage, { 11, 6 }, { 12, 8 }, { 255 }, -1);
	

	// Testmatrix 02
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\binImageFromPaper.png", cv::IMREAD_GRAYSCALE);
	
	// Testmatrix 03
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\lena_512x512_8u.png", cv::IMREAD_GRAYSCALE);

	// Testmatrix 03-A
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\binImageFromLena01.png", cv::IMREAD_GRAYSCALE);

	// Testmatrix 03-B
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\binImageFromLena02.png", cv::IMREAD_GRAYSCALE);

	// Testmatrix 04
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\lena_1172x1172_8u.png", cv::IMREAD_GRAYSCALE);

	// Testmatrix 05
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\Fehler01_16x16_bin.png", cv::IMREAD_GRAYSCALE);

	// Testmatrix 06
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\Fehler02_16x16_bin.png", cv::IMREAD_GRAYSCALE);

	// Testmatrix 07
	//cv::Mat binImage = cv::imread("C:\\Users\\Soeren\\Desktop\\binImage_8way.png", cv::IMREAD_GRAYSCALE);
	
	/**/
	cv::cuda::GpuMat binImageGpu(binImage);

	ccl::ConnectedComponentLabeling cclObj{ binImage.size() };
	auto res = cclObj.perform(binImageGpu, ccl::ConnectedComponentLabeling::Connectivity::eight);
	bool err = compareWithOpenCV(binImage, res);
	if (err) {
		std::cout << "ERROR\n";
	}

	/*
	// Endurance test
	cv::Mat binImage(cv::Size2i{ 48, 48 }, CV_8UC1);
	ccl::ConnectedComponentLabeling cclObj{ binImage.size() };
	cv::cuda::GpuMat binImageGpu(binImage);

	size_t imgCounter = 0;
	while(true) {
		
		cv::randu(binImage, 0, 2);
		binImage = binImage * 255;

		binImageGpu.upload(binImage);

		auto res = cclObj.perform(binImageGpu, ccl::Connectivity::eight);

		bool err = compareWithOpenCV(binImage, res);

		if (err) {
			cv::imwrite("D:\\imgCuda\\binimg_" + std::to_string(imgCounter) + ".png", binImage);
			++imgCounter;
		}
	}
	*/
	
	return 0;
}