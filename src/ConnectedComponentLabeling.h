#ifndef CONNECTEDCOMPONENTLABELING_H
#define CONNECTEDCOMPONENTLABELING_H

#include <opencv2/core/cuda.hpp>
#include <cuda.h>
#include <vector>

// To-Do:
//	- Error Handling (e.g. Memory Allocation)
//  - Documentation
//  - I use for development the openCV GpuMat Wrapper which limits the max. image size to process to max. signed int pixels (label id assigning).
//	  Use an unsigned in 2d-Array from cuda and get rid of overloaded findRoot() and merge().
//  - Write automatic tests
//  - To compress labels in the binary image the root label has to be marked (changes the pixel value). Think about another approach without manipulation of the input image.
//  - Optimization? Flow of results data (download, stream sync...)

namespace ccl{

	struct ResultFromCCL {
		cv::Rect2i boundingBox;
		int defectivePixels;
	};

	struct ConnectedComponentLabeling{
		
		enum class Connectivity {
			four, // 4-way connected neighborhood
			eight // 8-way connected neighborhood
		};

		/**
		 * The constructor allocates enough memory on host and
		 * device to evaluate images with size smaller than
		 * maxSize without reallocations. Otherwise throws
		 * an exception.
		 * 
		 * Limitation: maxSize.area() <= 2,147,483,647 pixels since
		 * member variable memoryLabelImg is of type CV_32SC1.
		 * 
		 * @param maxSize Maximum image size (in pixel).
		 */
		explicit ConnectedComponentLabeling(cv::Size maxSize);

		~ConnectedComponentLabeling();
		
		/**
		 * Computes the stats of the connected components in
		 * the given boolean GpuMat.
		 *
		 * @param binImg Binary image with one channel (see constructor for maximum size).
		 * Background pixel value: 0
		 * Foreground pixel value: Greater 0 and the same in the whole image
		 * @param connectiviy 4-way or 8-way connectivity respectively.
		 * @param cudaStream Stream for the asynchronous version.
		 * @return Result of Connected Components Analysis (bounding boxes
		 * and defective pixels)
		 */
		std::vector<ResultFromCCL> perform(const cv::cuda::GpuMat& binImg,
										   const Connectivity connectiviy = Connectivity::four,
										   cv::cuda::Stream cudaStream = cv::cuda::Stream::Null());

	private:
		/**
		* Preallocated memory that is used to create Label Map
		* during processing.
		*/
		cv::cuda::GpuMat memoryLabelImg;

		/**
		* Preallocated memory to extract ROIs from Label Map on host and device.
		*/
		int bufferSizeResults;
		int *h_xStart, *h_yStart, *h_xEnd, *h_yEnd, *h_defectivePixel;
		int *d_xStart, *d_yStart, *d_xEnd, *d_yEnd, *d_defectivePixel;

		int *h_numberOfConnectedCompontents, *d_numberOfConnectedCompontents;

	};
}
#endif