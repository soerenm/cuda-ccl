# cuda-ccl

Implementation of a parallel CCL algorithm based on the paper [[1]](#bibliography) using CUDA and openCV.

# Bibliography

J. Chen, K. Nonaka, H. Sankoh, R. Watanabe, H. Sabirin and S. Naito, "Efficient Parallel Connected Component Labeling With a Coarse-to-Fine Strategy," in IEEE Access, vol. 6, pp. 55731-55740, 2018.

J. Chen, K. Nonaka, R. Watanabe, H. Sankoh, H. Sabirin and S. Naito, "Efficient Parallel Connected Components Labeling with a Coarse-to-fine Strategy", arXiv, eprint 1712.09789, 2018.

Jun Chen, Qiang Yao, Houari Sabirin, Keisuke Nonaka, Hiroshi Sankoh and Sei Naito, "An Optimized Union-Find Algorithm for Connected Components Labeling Using GPUs", arXiv, eprint 1708.08180, 2017.

Victor Oliveira and Roberto Lotufo, "A Study on Connected Components Labeling algorithms using GPUs", SIB-GRAPI, 2010. 